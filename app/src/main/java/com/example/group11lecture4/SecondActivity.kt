package com.example.group11lecture4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()

    }
    private fun init(){
        Glide.with(this).load("https://zooclub.ambebi.ge/images/stories/2014/aprili/dzaglebi/rotveileri-i1%201.jpg").into(ImageView1)
        Glide.with(this).load("http://s44.radikal.ru/i103/0912/0c/31380cd7a33b.jpg").into(ImageView2)
        Glide.with(this).load("https://upload.wikimedia.org/wikipedia/commons/0/00/Two_adult_Guinea_Pigs_%28Cavia_porcellus%29.jpg").into(ImageView3)
        Glide.with(this).load("https://i1.wp.com/turtle-home.net/templates/Techno-Co/img02/kak-soderzhat-cherepahu-v-domashnih-uslovijah.JPG").into(ImageView4)


    }
}
