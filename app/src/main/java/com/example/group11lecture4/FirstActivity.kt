package com.example.group11lecture4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()

    }

    private fun init() {
        logInButton.setOnClickListener {
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()) {
                Toast.makeText(this, "Log In Success", Toast.LENGTH_LONG).show()
                val intent = Intent(this,SecondActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Log In Failed", Toast.LENGTH_LONG).show()
            }

        }
        RegisterButton.setOnClickListener{
            val intent = Intent(this,ThirdActivity::class.java)
            startActivity(intent)
        }
    }

}

